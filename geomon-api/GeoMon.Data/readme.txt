﻿dotnet commands to work with EF
-------------------------------

# Install Entity Framework Core

dotnet add package Microsoft.EntityFrameworkCore.InMemory --version 2.2.4
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
dotnet add package Microsoft.EntityFrameworkCore.Sqlite
dotnet add package Microsoft.EntityFrameworkCore.Design

# Create the database

dotnet ef migrations add InitialCreate
dotnet ef migrations add InitialCreate --context DataContext --output-dir Migrations/MySqlMigrations
dotnet ef database update

----------------------------------------------------------------------------------------------------------------
dotnet restore
dotnet run

----------------------------------------------------------------------------------------------------------------

Executen following commands in "Package Manager Console"

1 Add new migration

add-migration v1.0.0 -Context DataContext -OutputDir Migrations\MySqlMigrations

2 Update database:

update-database -Context DataContext