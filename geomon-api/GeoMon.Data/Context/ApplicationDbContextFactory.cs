﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace GeoMon.Data.Context
{
    public class ApplicationDbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        /// <summary>
        /// Environment name variable
        /// </summary>
        public static string EnvironmentName { get; } = new ConfigurationBuilder()
            .AddEnvironmentVariables()
            .Build().GetValue<string>("ASPNETCORE_ENVIRONMENT");

        public DataContext CreateDbContext(string[] args)
        {
            var configuration  = new ConfigurationBuilder()
           .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
           .AddJsonFile($"appsettings.{EnvironmentName}.json", optional: true, reloadOnChange: true)
           .AddEnvironmentVariables()
           .Build();

            var builder = new DbContextOptionsBuilder<DataContext>();
            var connectionString = configuration.GetConnectionString("Database");

            builder.UseMySql(connectionString, optionsBuilder => 
                optionsBuilder.MigrationsAssembly(typeof(DataContext).GetTypeInfo().Assembly.GetName().Name));

            return new DataContext(builder.Options);
        }
    }
}
