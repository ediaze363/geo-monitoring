using GeoMon.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace GeoMon.Data.Context
{
    /// <summary>
    /// Database context interact with specific provider
    /// See https://docs.microsoft.com/en-us/ef/core/get-started/aspnetcore/new-db?tabs=netcore-cli
    /// Fluent Api
    /// https://www.learnentityframeworkcore.com/configuration/fluent-api
    /// https://www.tutorialspoint.com/entity_framework/entity_framework_fluent_api.htm
    /// </summary>
    public class DataContext : DbContext, IBaseDbContext
    {
        public virtual DbSet<User> Users { get; set; }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
        }
    }
}
