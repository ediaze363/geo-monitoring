﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace GeoMon.Data.Context
{
    /// <summary>
    /// Database provider manager to setting up all specific configuration for each provider
    /// </summary>
    public static class DataProviderManager
    {
        /// <summary>
        /// Facade method to configure each provider 
        /// </summary>
        /// <param name="services"></param>
        /// <param name="databaseProvider"></param>
        /// <param name="connectionString"></param>
        public static void Configure(IServiceCollection services, string databaseProvider, string connectionString)
        {
            switch (databaseProvider)
            {
                case "Microsoft.EntityFrameworkCore.SqlServer":
                    services.AddDbContext<DataContext>(optionsBuilder => optionsBuilder
                        .UseSqlServer(connectionString, providerOptions => providerOptions.CommandTimeout(60))
                        .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking));
                    break;
                case "Pomelo.EntityFrameworkCore.MySql":
                default:
                    services.AddDbContext<DataContext>(options => options.UseMySql(connectionString));
                    break;
            }
        }
    }
}
