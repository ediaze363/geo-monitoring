using AutoMapper;
using GeoMon.Domain.Dtos;
using GeoMon.Domain.Models;

namespace GeoMon.Data.Mappers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
        }
    }
}