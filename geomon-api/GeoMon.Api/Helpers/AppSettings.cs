namespace GeoMon.Api.Helpers
{
    /// <summary>
    /// Configuration mapper for appsettings.json file
    /// </summary>
    public class AppSettings
    {
        /// <value>
        /// Private key for encryption.
        /// see tutorial http://jasonwatmore.com/post/2018/10/29/angular-7-user-registration-and-login-example-tutorial
        /// </value>
        public string Secret { get; set; }
        /// <value>
        /// Full connection string provider.
        /// See providers list https://docs.microsoft.com/en-us/ef/core/providers/index
        /// </value>
        public string DatabaseProvider { get; set; }
    }
}