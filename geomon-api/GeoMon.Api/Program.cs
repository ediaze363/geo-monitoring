﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;
using System;
using System.IO;

namespace GeoMon.Api
{
    /// <summary>
    /// Initial class
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Environment name variable
        /// </summary>
        public static string EnvironmentName { get; } = new ConfigurationBuilder()
            .AddEnvironmentVariables()
            .Build().GetValue<string>("ASPNETCORE_ENVIRONMENT");

        /// <summary>
        /// Configuration variable
        /// </summary>
        public static IConfiguration Configuration { get; } = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            .AddJsonFile($"appsettings.{EnvironmentName}.json", optional: true, reloadOnChange: true)
            .AddEnvironmentVariables()
            .Build();

        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(Configuration)
                .CreateLogger();

            try
            {
                Log.Information("Configuring Logging with level {LogLevel}",
                    Enum.Parse(typeof(LogEventLevel), Configuration.GetValue<string>("Serilog:LevelSwitches:$mySwitch")));
                CreateWebHostBuilder(args).Build().Run();
            }
            catch (Exception exception)
            {
                Log.Fatal(exception, "Host terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
