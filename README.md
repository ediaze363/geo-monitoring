# Geology Monitoring #

Repositorio Front-End y Back-end para el monitoreo geologíco de recursos. Por defecto este proyecto corre en windows pero con unos pocos cambios puede correr en Mac y Linux también.

## Front-End ##

El Front-End de la aplicación está en Angular y puede ser hospedado en IIS o cualquier otro tipo de host que sea compatible con Angular.

### Requerimientos de Software ###

* Nodejs 10.15.3 o superior
* npm 4.6.1 o superior
* Angular 7.3.8

### Comandos Front-End ###

Verificar la versión de Nodejs instalada
```
$ node -v # => displays your Node.js version
$ npm -v # => displays your npm version
```

Verificar la versión de Angular
```
$ ng version
```

Instalar la versión de Typescript
```
npm install -g typescript@latest
```

Instalar la versión más reciente de Angular
```
$ npm uninstall -g @angular/cli angular-cli 
$ npm cache clean 
$ npm install -g @angular/cli@latest 
```

Correr el servicio de front-end
```
$ ng serve
```

## Back-End ##

### Requerimientos de Software ###

* .Net Core SDK 2.2

### Comandos Back-End ###

Obtener el .Net Core SDK por defecto y las demás versiones instaladas
```
C:\>dotnet --info
```

## Referencias ##

* [Download .NET SDKs for Visual Studio](https://dotnet.microsoft.com/download/visual-studio-sdks?utm_source=getdotnetsdk&utm_medium=referral) - Para VS descargar el SDK
* [How To Target .NET Core 2.2](https://dotnetcoretutorials.com/2018/11/18/how-to-target-net-core-2-2/) - Cuando el VS no reconoce la versión más reciente de SDK
* [Tutorial de Angular de Sitepoint](https://www.sitepoint.com/angular-2-tutorial/) - Recomendaciones buenas prácticas y utilización de pruebas
* [Angular Style Guide](https://angular.io/guide/styleguide) - Guías de nombramiento para Angular
* [C# .Net Core Naming Guidelines](https://docs.microsoft.com/en-us/dotnet/standard/design-guidelines/naming-guidelines) - Guías de nombramiento para C#
* [VS 2019 Community](https://visualstudio.microsoft.com/downloads/) - Download Preview
* [Dynamic Forms](https://angular.io/guide/dynamic-form) - Formularios dinámicos con TS
* [Add Swagger to ASP.NET Core 2.0 Web API](https://www.talkingdotnet.com/add-swagger-to-asp-net-core-2-0-web-api/) - Documentación de API
* [Getting Started with EF Core on ASP.NET Core with a New database](https://docs.microsoft.com/en-us/ef/core/get-started/aspnetcore/new-db?tabs=netcore-cli) - EF tutorial
* [Database Providers](https://docs.microsoft.com/en-us/ef/core/providers/index) - EF Database providers
* [10 VS Code Extensions for FrontEnd Developers in 2019] (https://blog.bitsrc.io/top-10-vs-code-extensions-for-frontend-developers-in-2018-7992282db2ca) - Visual Studio Code