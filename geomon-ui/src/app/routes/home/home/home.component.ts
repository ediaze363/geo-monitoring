import { Component, OnInit } from '@angular/core';
import { TranslatorService } from '../../../core/translator/translator.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    selectedLanguage: any;

    constructor(
      private translator: TranslatorService
    ) {
      this.selectedLanguage = this.getLangs()[0].code;
      this.setLang(this.selectedLanguage);
    }

    ngOnInit() {
    }

    getLangs() {
      return this.translator.getAvailableLanguages();
    }

    setLang(value: string) {
      this.translator.useLanguage(value);
    }
}
