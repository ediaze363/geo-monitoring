import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../core/settings/settings.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { Router } from '@angular/router';
import { AuthenticationService, UserService, AlertService } from '../../../core/_services';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    registerForm: FormGroup;
    passwordForm: FormGroup;
    app: any;
    loading = false;
    submitted = false;

    get form() { return this.registerForm.controls; }

    constructor(
        public settings: SettingsService,
        private formBuilder: FormBuilder,
        private router: Router,
        private authenticationService: AuthenticationService,
        private userService: UserService,
        private alertService: AlertService
    ) {
        // redirect to home if already logged in
        if (this.authenticationService.currentUserValue) {
          this.router.navigate(['/']);
        }
    }

    ngOnInit() {
      const password = new FormControl('', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9]{6,10}$')]));
      const certainPassword = new FormControl('', [Validators.required, CustomValidators.equalTo(password)]);

      this.passwordForm = this.formBuilder.group({
          'password': password,
          'confirmPassword': certainPassword
      });

      this.registerForm = this.formBuilder.group({
          'email': [null, Validators.compose([Validators.required, CustomValidators.email])],
          'accountagreed': [null, Validators.required],
          'passwordGroup': this.passwordForm
      });

      this.app = {
        name: this.settings.getAppSetting('name')
      };
    }

    submitForm($ev, value: any) {
        $ev.preventDefault();
// tslint:disable-next-line: forin
        for (const field in this.registerForm.controls) {
            this.registerForm.controls[field].markAsTouched();
        }
// tslint:disable-next-line: forin
        for (const field in this.passwordForm.controls) {
            this.passwordForm.controls[field].markAsTouched();
        }

        if (this.registerForm.valid) {
            console.log('Valid!');
            console.log(value);
        }
    }
}
