import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../core/settings/settings.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService, AlertService } from '../../../core/_services';
import { first } from 'rxjs/operators';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    app: any;

    get form() { return this.loginForm.controls; }

    constructor(
      public settings: SettingsService,
      public formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private authenticationService: AuthenticationService,
      private alertService: AlertService
    ) {
        // redirect to home if already logged in
        if (this.authenticationService.currentUserValue) {
            this.router.navigate(['/']);
        }
    }

    ngOnInit() {
      this.loginForm = this.formBuilder.group({
        'email': [null, Validators.compose([Validators.required])],
        'password': [null, Validators.required]
      });

      this.app = {
        name: this.settings.getAppSetting('name')
      };

      // get return url from route parameters or default to '/'
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    submitForm($ev, value: any) {
        $ev.preventDefault();
        this.submitted = true;

// tslint:disable-next-line: forin
        for (const field in this.loginForm.controls) {
            this.loginForm.controls[field].markAsTouched();
        }

        // stop here if form is invalid
        if (!this.loginForm.valid) {
            console.log('Invalid!');
            console.log(value);
            return;
        }

        this.loading = true;
        this.authenticationService.login(this.form.email.value, this.form.password.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate([this.returnUrl]);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
