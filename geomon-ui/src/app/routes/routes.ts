import { LayoutComponent } from '../layout/layout.component';
import { LoginComponent } from './auth/login';
import { RegisterComponent } from './auth/register';
import { RecoverComponent } from './auth/recover';
import { AuthGuard } from '../core/_guards';

export const routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'recover', component: RecoverComponent },
  {
      path: '',
      component: LayoutComponent,
      canActivateChild: [AuthGuard],
      children: [
          { path: '', redirectTo: 'home', pathMatch: 'full' },
          { path: 'home', loadChildren: './home/home.module#HomeModule' }
      ]
  },

  // Not found
  { path: '**', redirectTo: 'home' }

];
