import { SettingsService } from './../../../core/settings/settings.service';
import { Component, OnInit } from '@angular/core';

import { UserblockService } from './userblock.service';

@Component({
    selector: 'app-userblock',
    templateUrl: './userblock.component.html',
    styleUrls: ['./userblock.component.scss']
})
export class UserblockComponent implements OnInit {
    user: any;
    constructor(
      public userblockService: UserblockService,
      public settings: SettingsService
      ) {

        this.user = {
            picture: settings.getUserSetting('picture'),
            job: settings.getUserSetting('job'),
            name: settings.getUserSetting('name')
        };
    }

    ngOnInit() {
    }

    userBlockIsVisible() {
        return this.userblockService.getVisibility();
    }

}
